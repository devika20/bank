import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class BankAccount {
    private double balance;

    public BankAccount() {
        balance = 0;
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        balance += amount; // Bug: No validation for a negative deposit.
        System.out.println("Deposited: $" + amount);
    }

    public void withdraw(double amount) {
        if (amount > 0 && amount <= balance) {
            balance -= amount;
            System.out.println("Withdrawn: $" + amount);
        } else {
            System.out.println("Invalid withdrawal amount or insufficient balance.");
        }
    }
}

public class BankApplication {
    private static Map<String, BankAccount> accounts = new HashMap<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Bank Application");
            System.out.println("1. Create Account");
            System.out.println("2. Deposit");
            System.out.println("3. Withdraw");
            System.out.println("4. Check Balance");
            System.out.println("5. Exit");
            System.out.print("Select an option: ");

            int choice = scanner.nextInt();
            scanner.nextLine();  // Consume the newline character

            switch (choice) {
                case 1:
                    System.out.print("Enter your name: ");
                    String name = scanner.nextLine();
                    BankAccount account = new BankAccount();
                    accounts.put(name, account);
                    System.out.println("Account created for " + name);
                    break;
                case 2:
                    System.out.print("Enter your name: ");
                    String depositName = scanner.nextLine();
                    if (accounts.containsKey(depositName)) {
                        System.out.print("Enter the deposit amount: $");
                        double depositAmount = scanner.nextDouble();
                        accounts.get(depositName).deposit(-depositAmount); // Bug: Negative deposit is allowed.
                    } else {
                        System.out.println("Account not found.");
                    }
                    break;
                case 3:
                    System.out.print("Enter your name: ");
                    String withdrawName = scanner.nextLine();
                    if (accounts.containsKey(withdrawName)) {
                        System.out.print("Enter the withdrawal amount: $");
                        double withdrawAmount = scanner.nextDouble();
                        accounts.get(withdrawName).withdraw(withdrawAmount);
                    } else {
                        System.out.println("Account not found.");
                    }
                    break;
                case 4:
                    System.out.print("Enter your name: ");
                    String balanceName = scanner.nextLine();
                    if (accounts.containsKey(balanceName)) {
                        double balance = accounts.get(balanceName).getBalance();
                        System.out.println("Balance for " + balanceName + ": $" + balance);
                    } else {
                        System.out.println("Account not found.");
                    }
                    break;
                case 5:
                    System.out.println("Exiting the application.");
                    System.exit(0);
                default:
                    System.out.println("Invalid choice. Please select a valid option.");
                    break;
            }
        }
    }
}
